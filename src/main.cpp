/*
 * Copyright (C) 2024  UBports Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * lomiri-calendar-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGuiApplication>
#include <QQmlEngine>
#include <QCoreApplication>
#include <QUrl>
#include <QDir>
#include <QQuickView>

#include <libintl.h>

#include "config.hpp"

int main(int argc, char *argv[])
{
    QGuiApplication *app = new QGuiApplication(argc, (char**)argv);
    app->setApplicationName("calendar.ubports");

    std::string domainDir = localeDirectory().toStdString();

    textdomain("lomiri-calendar-app");
    bindtextdomain("lomiri-calendar-app", domainDir.c_str());
    bind_textdomain_codeset ("lomiri-calendar-app", "UTF-8");

    QQuickView *view = new QQuickView();
    view->setSource(QUrl("qrc:/calendar.qml"));
    view->setResizeMode(QQuickView::SizeRootObjectToView);
    view->show();

    return app->exec();
}
