/*
 * Copyright (C) 2013-2014 Canonical Ltd
 *
 * This file is part of Lomiri Calendar App
 *
 * Lomiri Calendar App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Content 1.3
import QtOrganizer 5.0
import Qt.labs.settings 1.0
import Lomiri.Connectivity 1.0

import "dateExt.js" as DateExt
import "ViewType.js" as ViewType

MainView {
    id: mainView

    property bool displayWeekNumber: false;
    property bool displayLunarCalendar: false;
    property int reminderDefaultValue: 900;
    property int newEventDefaultLength:60;
    property int businessHourStart: 8;
    property int businessHourEnd: 16;
    readonly property bool syncInProgress: commonHeaderActions.syncInProgress
    signal themeChanged()


    function handleUri(uri)
    {
        if(uri !== undefined && uri !== "") {
            //work-around for bug #192, as Qt.openUrlExternally need 3 slashes to trigger something
            //lets add an optional 3rd slash
            var commands = uri.split(/:\/\/?\//)[1].split("=");
            if(commands[0].toLowerCase() === "eventid") {
                // calendar://eventid=??
                if( eventModel ) {
                    // qtorganizer:eds::<event-id>
                    var eventId = commands[1];
                    var prefix = "qtorganizer:eds::";
                    if (eventId.indexOf(prefix) < 0)
                        eventId  = prefix + eventId;

                    eventModel.showEventFromId(eventId);
                }
            } else if (commands[0].toLowerCase() === "startdate") {
                var date = new Date(commands[1])
                // this will be handled by Tabs.component.completed
                tabs.starttime = date.getTime()
            }
        }
    }

    function handleImport(transfer) {
        var files = [];
        for (let i=0; i < transfer.items.length; i++) {
            const item = transfer.items[i];
            console.log("received from ContentHub:", item.url.toString())
            files.push(item.url.toString());
        }

        tabs.isReady = false // prevent TypeErrors when going back to tabs view
        pageStack.push(Qt.resolvedUrl("ImportEventsDialog.qml"),{"files": files, "model": eventModel});
        pageStack.currentPage.finalized.connect(function() {
            transfer.finalize()
            pageStack.pop()
            tabs.isReady = true
        });
    }

    // Work-around until this branch lands:
    // https://code.launchpad.net/~tpeeters/ubuntu-ui-toolkit/optIn-tabsDrawer/+merge/212496
    //property bool windowActive: typeof window != 'undefined'
    //onWindowActiveChanged: window.title = i18n.tr("Calendar")

    // Argument during startup
    Arguments {
        id: args;

        // Example of argument: calendar:///new-event

        // IMPORTANT
        // Due to bug #1231558 you have to pass arguments BEFORE app:
        // qmlscene calendar:///new-event calendar.qml

        defaultArgument.help: i18n.tr("Calendar app accept four arguments: --starttime, --endtime, --newevent and --eventid. They will be managed by system. See the source for a full comment about them");
        defaultArgument.required: false;
        defaultArgument.valueNames: ["URL"]

        /* ARGUMENTS on startup
         * (no one is required)
         *
         * Create a new event
         * Keyword: newevent
         *
         * Create a new event. If starttime or endtime are set they are used to set start and end time of the new event.
         * It accepts no value.
         *
         *
         * Choose the view
         * Keyword: starttime
         *
         * If newevent has been called, starttime is the start time of event. Otherwise is the day on which app is focused on startup.
         * It accepts an integer value of the number of seconds since UNIX epoch in the UTC timezone.
         * 0 means today.
         *
         * Keyword: endtime
         *
         * If newevent is set it's the end time of the event, has to be > of starttime.
         * If newevent isn't set and startime is set, its value is used to choose the right view.
         * If neither of precendet flags are set, endtime is ignored.
         * It accepts an integer value of the number of seconds since UNIX epoch in the UTC timezone.
         *
         *
         * Open an existing event
         * Keyword: eventid (provisional)
         *
         * It takes a id of an event and open that event on full page
         */
        Argument {
            name: "eventid"
            required: false
            valueNames: ["EVENT_ID"]
        }
    }

    objectName: "calendar"

    width: units.gu(100)
    height: units.gu(80)
    focus: true
    Keys.forwardTo: [pageStack.currentPage]
    backgroundColor: theme.palette.normal.background
    anchorToKeyboard: false

    Connections {
        target: UriHandler
        onOpened: {
            handleUri(uris[0])
            if (tabs.starttime !== -1) {
                tabs.currentDay = new Date(tabs.starttime);
                if (tabs.selectedTabIndex != ViewType.ViewTypeDay)
                    tabs.selectedTabIndex = ViewType.ViewTypeDay
                else {
                    contentView.showDate(tabs.currentDay)
                }
                tabs.starttime = -1
            }
        }
    }

    Connections {
        id: chConnection
        target: ContentHub

        onImportRequested: {
            if (transfer.state === ContentTransfer.Charged) {
               handleImport(transfer)
            }
        }
    }

    /*
      Indicates network status
      see https://api-docs.ubports.com/sdk/apps/qml/Ubuntu.Connectivity/NetworkingStatus.html
    */
    property bool networkError //derived from Connectivity, only set false for desktop mode
    property var statusMap: ["Offline", "Connecting", "Online"]

    Connections {
       target: Connectivity
       onOnlineChanged: networkError = Connectivity.online ? false : true
    }

    // This is for wait that the app is load when newEvent is invoked by argument
    Timer {
        id: timer
        interval: 200; running: false; repeat: false
        onTriggered: {
            tabs.newEvent();
        }
    }

    // Load events after the app startup
    Timer {
        id: applyFilterTimer
        interval: 200; running: false; repeat: false
        onTriggered: {
            eventModel.applyFilterFinal();
        }
    }

    UnionFilter {
        id: itemTypeFilter
        DetailFieldFilter{
            id: eventFilter
            detail: Detail.ItemType;
            field: Type.FieldType
            value: Type.Event
            matchFlags: Filter.MatchExactly
        }

        DetailFieldFilter{
            id: eventOccurenceFilter
            detail: Detail.ItemType;
            field: Type.FieldType
            value: Type.EventOccurrence
            matchFlags: Filter.MatchExactly
        }
    }

    CollectionFilter{
        id: collectionFilter
    }

    InvalidFilter {
        id: invalidFilter
        objectName: "invalidFilter"
    }

    IntersectionFilter {
        id: mainFilter

        filters: [ collectionFilter, itemTypeFilter]
    }

    EventListModel{
        id: eventModel
        objectName: "calendarEventList"

        property bool isReady: false
        property alias collectionsToFilter: collectionFilter.ids

        function enabledColections()
        {
            var collectionIds = [];
            var collections = eventModel.getCollections();
            for(var i=0; i < collections.length ; ++i) {
                var collection = collections[i]
                if(collection.extendedMetaData("collection-selected") === true) {
                    collectionIds.push(collection.collectionId);
                }
            }
            return collectionIds
        }

        function delayedApplyFilter() {
            applyFilterTimer.restart();
        }

        function applyFilterFinal() {
            var collectionIds = enabledColections()
            collectionFilter.ids = collectionIds;
            filter = Qt.binding(function() { return mainFilter; })
            isReady = true
        }

        function showEventFromId(eventId) {
            if(eventId === undefined || eventId === "") {
                return;
            }

            var requestId = "";
            var callbackFunc = function(id,fetchedItems) {
                if( requestId === id && fetchedItems.length > 0 ) {
                    var event = fetchedItems[0]
                    contentView.showDate(event.startDateTime)
                    pageStack.push(Qt.resolvedUrl("EventDetails.qml"),{"event":event,"model": eventModel});
                }
                eventModel.onItemsFetched.disconnect( callbackFunc );
            }
            eventModel.onItemsFetched.connect( callbackFunc );
            requestId = eventModel.fetchItems(eventId);
        }

        // force this model to never disable the auto-update
        live: true
        active: true
        startPeriod: tabs.currentDay
        endPeriod: tabs.currentDay

        filter: invalidFilter

        onCollectionsChanged: {
            if (!isReady) {
                return
            }

            var collectionIds = enabledColections()
            var oldCollections = collectionFilter.ids
            var needsUpdate = false
            if (collectionIds.length != oldCollections.length) {
                needsUpdate = true
            } else {
                for(var i=oldCollections.length - 1; i >=0 ; i--) {
                    if (collectionIds.indexOf(oldCollections[i]) === -1) {
                        needsUpdate = true
                        break;
                    }
                }
            }

            if (needsUpdate) {
                eventModel.collectionsToFilter = collectionIds
                updateIfNecessary()
            }
        }

        Component.onCompleted: {
            delayedApplyFilter();

            if (args.values.eventid) {
                showEventFromId(args.values.eventid);
            }
        }
    }


    EventActions {
        id: commonHeaderActions
        settings: settings
    }

    Settings {
        id: settings
        property alias defaultViewIndex: tabs.selectedTabIndex
        property alias showWeekNumber: mainView.displayWeekNumber
        property alias showLunarCalendar: mainView.displayLunarCalendar
        property alias reminderDefaultValue: mainView.reminderDefaultValue
        property alias newEventDefaultLength: mainView.newEventDefaultLength
        property alias businessHourStart: mainView.businessHourStart
        property alias businessHourEnd: mainView.businessHourEnd
        property string selectedTheme: "System"

        function defaultViewIndexValue(fallback) {
            return defaultViewIndex != -1 ? defaultViewIndex : fallback
        }

    }

    Item{
        id: tabs
        anchors.fill: parent
        Keys.forwardTo: [tabs.currentPage]

        property int selectedTabIndex: -1

        onSelectedTabIndexChanged: {

            var comp = null

            switch (selectedTabIndex)
            {
            case ViewType.ViewTypeAgenda:
                comp = agendaViewComp;
                break
            case ViewType.ViewTypeDay:
                comp = dayViewComp;
                break
            case ViewType.ViewTypeWeek:
                comp = weekViewComp;
                break
            case ViewType.ViewTypeMonth:
                comp = monthViewComp;
                break
            case ViewType.ViewTypeYear:
                comp = yearViewComp;
                break
            default:
                comp = dayViewComp;
            }

            contentView.currentView = comp
        }

        property bool isReady: false
        property var currentDay: DateExt.today();
        property var selectedDay;

        // Arguments on startup
        property bool newevent: false;
        property real starttime: -1;
        property real endtime: -1;
        property string eventId;

        // //WORKAROUND: The new header api does not work with tabs check bug: #1539759
        property list<Action> tabsAction: [
            Action {
                objectName: "tab_agendaTab"
                name: "tab_agendaTab"
                text: i18n.tr("Agenda")
                iconName: !enabled ? "tick" : ""
                enabled: (tabs.selectedTabIndex !== ViewType.ViewTypeAgenda)
                onTriggered: tabs.selectedTabIndex = ViewType.ViewTypeAgenda
            },
            Action {
                objectName: "tab_dayTab"
                name: "tab_dayTab"
                text: i18n.tr("Day")
                iconName: !enabled ? "tick" : ""
                enabled: (tabs.selectedTabIndex !== ViewType.ViewTypeDay)
                onTriggered: tabs.selectedTabIndex = ViewType.ViewTypeDay
            },
            Action {
                objectName: "tab_weekTab"
                name: "tab_weekTab"
                text: i18n.tr("Week")
                iconName: !enabled ? "tick" : ""
                enabled: (tabs.selectedTabIndex !== ViewType.ViewTypeWeek)
                onTriggered:  tabs.selectedTabIndex = ViewType.ViewTypeWeek
            },
            Action {
                objectName: "tab_monthTab"
                name: "tab_monthTab"
                text: i18n.tr("Month")
                iconName: !enabled ? "tick" : ""
                enabled: (tabs.selectedTabIndex !== ViewType.ViewTypeMonth)
                onTriggered:  tabs.selectedTabIndex = ViewType.ViewTypeMonth
            },
            Action {
                objectName: "tab_yearTab"
                name: "tab_yearTab"
                text: i18n.tr("Year")
                iconName: !enabled ? "tick" : ""
                enabled: (tabs.selectedTabIndex !== ViewType.ViewTypeYear)
                onTriggered:  tabs.selectedTabIndex = ViewType.ViewTypeYear
            }
        ]

    }


    PageStack {
        id: pageStack
    }

    Item {
        id: contentView
        anchors.fill: pageStack

        property alias currentView: loader.sourceComponent;

        function showDate(date) {
            var currentPage = loader.item
            if (currentPage && currentPage.showDate) {
                currentPage.showDate(date)
            }
        }

        Loader {
            id: loader
            anchors.fill: parent
            active: tabs.isReady
        }
    }

    Item {
        id: leftColumnContentWithPadding
        width: Math.ceil(childrenRect.width/units.gu(1))*units.gu(1) + units.gu(1)

        Label {
            id: localizedTimeLabelTemplate
            visible: false
            fontSize: "small"
            text: Qt.formatTime(new Date(0,0,0,12), Qt.SystemLocaleShortDate)
        }

        Repeater {
            model: i18n.tr("All Day").split(" ")

            Label {
                text: modelData
                visible: false
                fontSize: "small"
            }
        }
    }


    Component {
        id: yearViewComp

        YearView {

            function showDate(date)
            {
                refreshCurrentYear(date.getFullYear())
            }

            reminderValue: mainView.reminderDefaultValue
            model: eventModel.isReady ? eventModel : null
            bootomEdgeEnabled: true
            displayLunarCalendar: mainView.displayLunarCalendar

            onMonthSelected: {
                var now = DateExt.today();
                if ((date.getMonth() === now.getMonth()) &&
                    (date.getFullYear() === now.getFullYear())) {
                    tabs.currentDay = now;
                } else {
                    tabs.currentDay = date.midnight();
                }
                tabs.selectedTabIndex = ViewType.ViewTypeMonth;
            }


            Component.onCompleted: {
                showDate(tabs.currentDay)
                // lately subscribe to change
                currentYearChanged.connect(function() { tabs.currentDay = new Date(currentYear, 1, 1)  })
            }
        }
    }

    Component {
        id: monthViewComp

        MonthView {

            function showDate(date)
            {
                anchorDate = new Date(date.getFullYear(),
                                      date.getMonth(),
                                      1,
                                      0, 0, 0)
            }

            reminderValue: mainView.reminderDefaultValue
            model: eventModel.isReady && eventModel.collectionsToFilter ? eventModel : null
            displayLunarCalendar: mainView.displayLunarCalendar

            onWeekSelected: {
                tabs.currentDay = date
                tabs.selectedTabIndex = ViewType.ViewTypeWeek
            }

            onDateSelected: {
                tabs.currentDay = date
                tabs.selectedTabIndex = ViewType.ViewTypeDay
            }

            Component.onCompleted: {
                showDate(tabs.currentDay)
                // lately subscribe to change
                currentDateChanged.connect(function() { tabs.currentDay = currentDate  })
            }
       }
    }

    Component {
        id: weekViewComp

        WeekView {

            function showDate(date)
            {
                var dateGoTo = new Date(date)
                if (!anchorDate ||
                    (dateGoTo.getFullYear() != anchorDate.getFullYear()) ||
                    (dateGoTo.getMonth() != anchorDate.getMonth()) ||
                    (dateGoTo.getDate() != anchorDate.getDate())) {
                    anchorDate = new Date(dateGoTo)
                }
                delayScrollToDate(dateGoTo)
            }

            reminderValue: mainView.reminderDefaultValue
            model: eventModel.isReady && eventModel.collectionsToFilter ? eventModel : null
            displayLunarCalendar: mainView.displayLunarCalendar

            onHighlightedDayChanged: {
                if (highlightedDay)
                    tabs.currentDay = highlightedDay
                else
                    tabs.currentDay = currentFirstDayOfWeek
            }

            onDateSelected: {
                tabs.currentDay = date;
                tabs.selectedTabIndex = ViewType.ViewTypeDay
            }

            onPressAndHoldAt: {
                bottomEdgeCommit(date, allDay)
            }

            Component.onCompleted: {
                showDate(tabs.currentDay)
                // lately sbscribe to change
                currentFirstDayOfWeekChanged.connect(function() { tabs.currentDay = currentFirstDayOfWeek  })
            }
        }
    }

    Component {
        id: dayViewComp

        DayView {

            function showDate(date)
            {
                var dateGoTo = new Date(date)
                if (!currentDate ||
                    (dateGoTo.getFullYear() !== currentDate.getFullYear()) ||
                    (dateGoTo.getMonth() !== currentDate.getMonth()) ||
                    (dateGoTo.getDate() !== currentDate.getDate())) {
                    anchorDate = new Date(dateGoTo)
                }
                delayScrollToDate(dateGoTo)
            }

            reminderValue: mainView.reminderDefaultValue
            model: eventModel.isReady && eventModel.collectionsToFilter ? eventModel : null
            displayLunarCalendar: mainView.displayLunarCalendar

            onDateSelected: {
                tabs.currentDay = date
            }

            onPressAndHoldAt: {
                bottomEdgeCommit(date, allDay)
            }

            Component.onCompleted: {
                showDate(tabs.currentDay)
                currentDateChanged.connect(function() { tabs.currentDay = currentDate  })
            }
        }
    }

    Component {
        id: agendaViewComp

        AgendaView {

            reminderValue: mainView.reminderDefaultValue
            model: eventModel.isReady && eventModel.collectionsToFilter ? eventModel : null

            onDateSelected: {
                tabs.currentDay = date;
                tabs.selectedTabIndex = ViewType.ViewTypeDay
            }
        }
    }

    ThemeColors {
        id:calenderThemeColors
    }

    /*
      Sets the system theme according to the theme selected
      under settings.
    */
    function setCurrentTheme() {
        if (settings.selectedTheme == "System") {
          theme.name = "";
        } else if (settings.selectedTheme == "SuruDark") {
          theme.name = "Lomiri.Components.Themes.SuruDark"
        } else if (settings.selectedTheme == "Ambiance") {
          theme.name = "Lomiri.Components.Themes.Ambiance"
        } else {
          theme.name = "";
        }
    }

    onThemeChanged: setCurrentTheme()

    function newEvent() {
        var startDate = new Date();
        var endDate = new Date();
        var startTime;
        var endTime;

        if (starttime === 0) { // startime 0 means now
            if (endtime !== -1) { // If also endtime has been invoked
                endTime = parseInt(endtime);
                if (endTime > startDate) // If endtime is after startime
                    endDate = new Date(endTime);
            }
        }
        else if (starttime !== -1) { // If starttime has been invoked
            startTime = parseInt(starttime);
            startDate = new Date(startTime);
            if (endtime !== -1) { // If --endtime has been invoked
                endTime = parseInt(endtime);
                if (endTime > startDate)
                    endDate = new Date(endTime);
            }
        }
        //pageStack.push(Qt.resolvedUrl("NewEvent.qml"),{"startDate": startDate, "endDate": endDate, //"model":eventModel});
    }

    // This function calculate the difference between --endtime and --starttime and choose the better view
    function calculateDifferenceStarttimeEndtime(startTime, endTime) {
        var minute = 60 * 1000;
        var hour = 60 * minute;
        var day = 24 * hour;
        var month = 30 * day;

        var difference = endTime - startTime;

        if (difference > month)
            return ViewType.ViewTypeYear;   // Year view
        else if (difference > 7 * day)
            return ViewType.ViewTypeMonth;   // Month view}
        else if (difference > day)
            return ViewType.ViewTypeWeek;   // Week view
        else
            return ViewType.ViewTypeDay;   // Day view
    }

    // This function parse the argument
    function parseArguments(url) {
        var newevenpattern= new RegExp ("newevent");
        var starttimepattern = new RegExp ("starttime=\\d+");
        var endtimepattern = new RegExp ("endtime=\\d+");
        var eventIdpattern = new RegExp ("eventId=.*")
        var urlpattern = new RegExp("calendar://.*")

        if (urlpattern.test(url)) {
            handleUri(url)
            return
        }

        tabs.newevent = newevenpattern.test(url);

        if (starttimepattern.test(url))
            tabs.starttime = parseInt(url.match(/starttime=(\d+)/)[1]);

        if (endtimepattern.test(url))
            tabs.endtime = parseInt(url.match(/endtime=(\d+)/)[1]);

        if (eventIdpattern.test(url))
            tabs.eventId = url.match(/eventId=(.*)/)[1];
    }

    Keys.onTabPressed: {
        if( event.modifiers & Qt.ControlModifier) {
            var currentTab = tabs.selectedTabIndex;
            currentTab ++;
            if( currentTab >= tabs.tabChildren.length){
                currentTab = 0;
            }
            tabs.selectedTabIndex = currentTab;
        }
    }

    Keys.onBacktabPressed: {
        if( event.modifiers & Qt.ControlModifier) {
            var currentTab = tabs.selectedTabIndex;
            currentTab --;
            if( currentTab < 0){
                currentTab = tabs.tabChildren.length -1;
            }
            tabs.selectedTabIndex = currentTab;
        }
    }

    Component.onCompleted: {

        setCurrentTheme();

        let tabIndexToSelect = -1

        // If an url has been set
        if (args.defaultArgument.at(0)) {
            tabs.currentDay = new Date()
            parseArguments(args.defaultArgument.at(0))
            // If newevent has been called on startup
            if (tabs.newevent) {
                timer.running = true;
            }
            else if (tabs.starttime !== -1) { // If no newevent has been setted, but starttime
                tabs.currentDay = new Date(tabs.starttime);
                // If also endtime has been settend
                if (tabs.endtime !== -1) {
                    tabIndexToSelect = calculateDifferenceStarttimeEndtime(tabs.startTime, tabs.endTime);
                }
                else {
                    // If no endtime has been setted, open the starttime date in day view
                    tabIndexToSelect = ViewType.ViewTypeDay;
                }
            } // End of else if (starttime)
            else if (tabs.eventId !== "") {
                var prefix = "qtorganizer:eds::";
                if (tabs.eventId.indexOf(prefix) < 0)
                    tabs.eventId  = prefix + tabs.eventId;

                eventModel.showEventFromId(tabs.eventId);
            }
            else {
                // Due to bug #1231558 {if (args.defaultArgument.at(0))} is always true
                // After the fix we can delete this else
                tabIndexToSelect = settings.defaultViewIndexValue(1)
            }
        } // End of if about args.values
        else {
            tabIndexToSelect = settings.defaultViewIndexValue(1)
        }
        tabs.starttime = -1
        tabs.endtime = -1
        tabs.eventId = ""
        pageStack.push(contentView)
        tabs.selectedTabIndex = tabIndexToSelect
        tabs.isReady = true

        // WORKAROUND: Due the missing feature on SDK, they can not detect if
        // there is a mouse attached to device or not. And this will cause the
        // bootom edge component to not work correct on desktop.
        // We will consider that  a mouse is always attached until it get implement on SDK.
        QuickUtils.mouseAttached = true
    } // End of Component.onCompleted:
}
