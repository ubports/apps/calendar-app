/*
 * Copyright (C) 2013-2016 Canonical Ltd
 *
 * This file is part of Lomiri Calendar App
 *
 * Lomiri Calendar App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtOrganizer 5.0
import Lomiri.Components 1.3
import "dateExt.js" as DateExt

PageWithBottomEdge {
    id: root
    objectName: "AgendaView"

    property var anchorDate: new Date()

    signal dateSelected(var date)

    function goToBeginning() {
        eventList.positionViewAtBeginning();
    }

    function hasEnabledCalendars() {
        var enabled_calendars = eventListModel.getCollections().filter( function( item ) {
            return item.extendedMetaData( "collection-selected" );
        } );

        return !!enabled_calendars.length;
    }

    Keys.forwardTo: [eventList]
    createEventAt: anchorDate

    // Page Header
    header: DefaultHeader {
        id: defaultHeader
        title: i18n.tr("Agenda")

        trailingActionBar {
            actions: [
                commonHeaderActions.settingsAction,
                commonHeaderActions.showCalendarAction,
                commonHeaderActions.reloadAction,
                commonHeaderActions.syncCalendarAction,
            ]
            numberOfSlots: 4
        }
    }

    // make sure that the model is updated after create a new event if it is marked as auto-update false
    onEventSaved: eventListModel.updateIfNecessary()
    onEventDeleted: eventListModel.updateIfNecessary()


    // ListModel to hold all events for upcoming 7days.
    EventListModel {
        id: eventListModel
        objectName: "agendaEventListModel"

        startPeriod: anchorDate.midnight();
        endPeriod: anchorDate.addDays(7).endOfDay()
        filter: model.filter
        active: root.active
        sortOrders: [
            SortOrder{
                blankPolicy: SortOrder.BlanksFirst
                detail: Detail.EventTime
                field: EventTime.FieldStartDateTime
                direction: Qt.AscendingOrder
            }
        ]
    }

    // spinner. running while agenda is loading.
    ActivityIndicator {
        z:2
        visible: running
        running: eventListModel.isLoading
        anchors.centerIn: parent
    }

    // Label to be shown when there is no upcoming events or if no calendar is selected.
    Label {
        id: noEventsOrCalendarsLabel
        anchors.centerIn: parent
        visible: (eventList.itemCount === 0) && !eventListModel.isLoading
        text: !root.hasEnabledCalendars() ? i18n.tr("You have no calendars enabled") : i18n.tr( "No upcoming events" )
    }

    // button to be shown when no calendar is selected (onClick will take user to list of all calendars)
    Button {
        anchors {
            top: noEventsOrCalendarsLabel.bottom;
            horizontalCenter: noEventsOrCalendarsLabel.horizontalCenter;
            topMargin: units.gu(1.5)
        }
        color: LomiriColors.orange
        visible: !root.hasEnabledCalendars()
        text: i18n.tr( "Enable calendars" )

        onClicked: {
            pageStack.push(Qt.resolvedUrl("CalendarChoicePopup.qml"),{"model": model});
            pageStack.currentPage.collectionUpdated.connect(model.delayedApplyFilter);
        }
    }

    // Main ListView with all upcoming events.
    ListView {
        id: eventList
        objectName: "eventList"

        anchors{
            fill: parent
            topMargin: defaultHeader.height
            bottomMargin: root.bottomEdgeHeight
        }
        visible: eventListModel.itemCount > 0
        model: eventListModel
        delegate: AgendaEventDelegate {
            eventModel: eventListModel
            onDateClicked: {
                Haptics.play()
                dateSelected(startDateTime);
            }

            onEventClicked: {
                Haptics.play()
                pageStack.push(Qt.resolvedUrl("EventDetails.qml"), {"event":event,"model":eventListModel});
            }
        }
    }

    // Scrollbar
    Scrollbar{
        flickableItem: eventList
        align: Qt.AlignTrailing
    }

}
