/*
 * Copyright (C) 2014 Canonical Ltd
 *
 * This file is part of Lomiri Calendar App
 *
 * Lomiri Calendar App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Lomiri Calendar App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.SyncMonitor 0.1
import Lomiri.Components.Popups 1.3
import Qt.labs.settings 1.0

Item {
    id: actionPool

    property alias showCalendarAction: _showCalendarAction
    property alias syncCalendarAction: _syncCalendarAction
    property alias settingsAction: _settingsAction
    property Settings settings
    readonly property bool syncInProgress: (syncMonitor.state !== "") && (syncMonitor.state === "syncing")

    onSyncInProgressChanged: {

        if (syncInProgress) {
            syncErrorData.clear()
        }
    }

    QtObject {
        id: syncErrorData
        property string account: ""
        property string error: ""
        property string service: ""

        function getLogin() {
            const idx = account.lastIndexOf('@')
            if (idx !== -1) {
                return account.substring(0, idx)
            } else {
                return account
            }
        }

        function getUrl() {
            const idx = account.lastIndexOf('@')
            if (idx !== -1) {
                return account.substring(idx+1, account.length)
            } else {
                return account
            }
        }

        function clear() {
            account = "";
            error = "";
            service = "";
        }
    }

    Action {
        id: _syncCalendarAction

        objectName: "syncbutton"
        iconSource: "../assets/sync.svg"
        // TRANSLATORS: Please translate this string  to 15 characters only.
        // Currently ,there is no way we can increase width of action menu currently.
        text: i18n.tr("Sync")
        onTriggered: syncMonitor.sync(["calendar"])
        enabled: syncMonitor.enabledServices ? syncMonitor.serviceIsEnabled("calendar") : false
        visible: syncMonitor.enabledServices ? true : false
    }

    SyncMonitor {
        id: syncMonitor
        onSyncError: {
            console.log('SyncError:', account, service, error)
            if (error != "canceled") {

                syncErrorData.account = account
                syncErrorData.error = error
                syncErrorData.service = service
            }
        }
    }

    Action{
        id: _showCalendarAction
        objectName: "calendarsbutton"
        name: "calendarsbutton"
        iconName: "calendar"
        text: i18n.tr("Calendars")
        onTriggered: {
            pageStack.push(Qt.resolvedUrl("CalendarChoicePopup.qml"),{"model":eventModel});
            pageStack.currentPage.collectionUpdated.connect(eventModel.delayedApplyFilter);
        }
    }

    Action{
        id: _settingsAction
        objectName: "settingsbutton"
        name: "calendarsbutton"
        iconName: "settings"
        text: i18n.tr("Settings")
        onTriggered: pageStack.push(Qt.resolvedUrl("SettingsPage.qml"), {"eventModel": eventModel,
                                                                         "settings": actionPool.settings});
    }

    states: [
        State {
            name: "syncing"
            when: actionPool.syncInProgress
            PropertyChanges {
                target: _syncCalendarAction
                iconSource: "../assets/sync-cancel.svg"
                text: i18n.tr("Syncing")
                onTriggered: syncMonitor.cancel(["calendar"])
            }
        },
        State {
            name: "syncError"
            when: !actionPool.syncInProgress && syncErrorData.error.length > 0
            PropertyChanges {
                target: _syncCalendarAction
                iconSource: "../assets/sync-error.svg"
                text: i18n.tr("Sync error")
                onTriggered: {
                    PopupUtils.open(syncErrorComp, actionPool, {"syncErrorData": syncErrorData})
                }
            }
        }
    ]

    Component {
        id: syncErrorComp
        Dialog {
            id: dialogue
            objectName: "syncErrorDialog"

            property QtObject syncErrorData;


            title: i18n.tr("Sync error")
            text: i18n.tr("An error occurred during synchronisation for account %1 on server %2").arg(syncErrorData.getLogin()).arg(syncErrorData.getUrl())


            Button {
                text: i18n.tr("Retry sync")
                onClicked: {
                    syncMonitor.sync(["calendar"])
                    PopupUtils.close(dialogue)
                }
            }

            Button {
                text: i18n.tr("Edit online account")
                onClicked: {
                    Qt.openUrlExternally("settings://system/online-accounts")
                    PopupUtils.close(dialogue)
                }
            }

            Button {
                text: i18n.tr("Cancel")
                onClicked: PopupUtils.close(dialogue)
            }
        }
    }
}
