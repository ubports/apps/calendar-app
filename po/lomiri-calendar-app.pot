# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the lomiri-calendar-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: lomiri-calendar-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-11-01 14:35+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: lomiri-calendar-app.desktop.in:8 src/qml/EventDetails.qml:358
#: src/qml/NewEvent.qml:682
msgid "Calendar"
msgstr ""

#: lomiri-calendar-app.desktop.in:9
msgid "A calendar for Lomiri which syncs with online accounts."
msgstr ""

#: lomiri-calendar-app.desktop.in:10
msgid "calendar;event;day;week;year;appointment;meeting;"
msgstr ""

#: src/qml/AgendaView.qml:50 src/qml/calendar.qml:386 src/qml/calendar.qml:567
msgid "Agenda"
msgstr ""

#: src/qml/AgendaView.qml:100
msgid "You have no calendars enabled"
msgstr ""

#: src/qml/AgendaView.qml:100
msgid "No upcoming events"
msgstr ""

#: src/qml/AgendaView.qml:112
msgid "Enable calendars"
msgstr ""

#: src/qml/AllDayEventComponent.qml:89 src/qml/TimeLineBase.qml:50
msgid "New event"
msgstr ""

#. TRANSLATORS: Please keep the translation of this string to a max of
#. 5 characters as the week view where it is shown has limited space.
#: src/qml/AllDayEventComponent.qml:148
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] ""
msgstr[1] ""

#. TRANSLATORS: the argument refers to the number of all day events
#: src/qml/AllDayEventComponent.qml:152
msgid "%1 all day event"
msgid_plural "%1 all day events"
msgstr[0] ""
msgstr[1] ""

#: src/qml/CalendarChoicePopup.qml:47 src/qml/EventActions.qml:103
msgid "Calendars"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:49 src/qml/SettingsPage.qml:51
msgid "Back"
msgstr ""

#. TRANSLATORS: Please translate this string  to 15 characters only.
#. Currently ,there is no way we can increase width of action menu currently.
#: src/qml/CalendarChoicePopup.qml:61 src/qml/EventActions.qml:79
msgid "Sync"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:61 src/qml/EventActions.qml:127
msgid "Syncing"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:103
msgid "Add online Calendar"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:217
msgid "Import events"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:230
msgid "Export calendar"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:248
msgid "Choose from"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:283
msgid "Calendar export"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:301
msgid "Select your calendar"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:344
msgid "Select all"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:361 src/qml/ColorPickerDialog.qml:55
#: src/qml/DeleteConfirmationDialog.qml:63
#: src/qml/EditEventConfirmationDialog.qml:52 src/qml/EventActions.qml:175
#: src/qml/NewEvent.qml:399 src/qml/OnlineAccountsHelper.qml:73
#: src/qml/RemindersPage.qml:88
msgid "Cancel"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:368
msgid "Proceed"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:403
msgid "Unable to deselect"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:404
msgid ""
"In order to create new events you must have at least one writable calendar "
"selected"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:406 src/qml/CalendarChoicePopup.qml:419
msgid "Ok"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:416
msgid "Network required"
msgstr ""

#: src/qml/CalendarChoicePopup.qml:417
msgid ""
"You are currently offline. In order to add online accounts you must have "
"network connection available."
msgstr ""

#: src/qml/ColorPickerDialog.qml:25
msgid "Select Color"
msgstr ""

#: src/qml/ContactChoicePopup.qml:37
msgid "No contact"
msgstr ""

#: src/qml/ContactChoicePopup.qml:96
msgid "Search contact"
msgstr ""

#: src/qml/DayView.qml:76 src/qml/MonthView.qml:51 src/qml/WeekView.qml:60
#: src/qml/YearView.qml:57
msgid "Today"
msgstr ""

#. TRANSLATORS: this is a time formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#. It's used in the header of the month and week views
#: src/qml/DayView.qml:129 src/qml/MonthView.qml:84 src/qml/WeekView.qml:159
msgid "MMMM yyyy"
msgstr ""

#: src/qml/DeleteConfirmationDialog.qml:31
msgid "Delete Recurring Event"
msgstr ""

#: src/qml/DeleteConfirmationDialog.qml:32
msgid "Delete Event"
msgstr ""

#. TRANSLATORS: argument (%1) refers to an event name.
#: src/qml/DeleteConfirmationDialog.qml:36
msgid "Delete only this event \"%1\", or all events in the series?"
msgstr ""

#: src/qml/DeleteConfirmationDialog.qml:37
msgid "Are you sure you want to delete the event \"%1\"?"
msgstr ""

#: src/qml/DeleteConfirmationDialog.qml:40
msgid "Delete series"
msgstr ""

#: src/qml/DeleteConfirmationDialog.qml:51
msgid "Delete this"
msgstr ""

#: src/qml/DeleteConfirmationDialog.qml:51 src/qml/NewEvent.qml:406
msgid "Delete"
msgstr ""

#: src/qml/EditEventConfirmationDialog.qml:29 src/qml/NewEvent.qml:394
msgid "Edit Event"
msgstr ""

#. TRANSLATORS: argument (%1) refers to an event name.
#: src/qml/EditEventConfirmationDialog.qml:32
msgid "Edit only this event \"%1\", or all events in the series?"
msgstr ""

#: src/qml/EditEventConfirmationDialog.qml:35
msgid "Edit series"
msgstr ""

#: src/qml/EditEventConfirmationDialog.qml:44
msgid "Edit this"
msgstr ""

#: src/qml/EventActions.qml:115 src/qml/SettingsPage.qml:49
msgid "Settings"
msgstr ""

#: src/qml/EventActions.qml:137 src/qml/EventActions.qml:154
msgid "Sync error"
msgstr ""

#: src/qml/EventActions.qml:155
msgid "An error occurred during synchronisation for account %1 on server %2"
msgstr ""

#: src/qml/EventActions.qml:159
msgid "Retry sync"
msgstr ""

#: src/qml/EventActions.qml:167
msgid "Edit online account"
msgstr ""

#. TRANSLATORS: the first argument (%1) refers to a start time for an event,
#. while the second one (%2) refers to the end time
#: src/qml/EventBubble.qml:139
msgid "%1 - %2"
msgstr ""

#: src/qml/EventDetails.qml:37 src/qml/NewEvent.qml:601
msgid "Event Details"
msgstr ""

#: src/qml/EventDetails.qml:41
msgid "Share"
msgstr ""

#: src/qml/EventDetails.qml:49
msgid "Edit"
msgstr ""

#: src/qml/EventDetails.qml:183 src/qml/TimeLineHeader.qml:66
#: src/qml/calendar.qml:723
msgid "All Day"
msgstr ""

#: src/qml/EventDetails.qml:402
msgid "Attending"
msgstr ""

#: src/qml/EventDetails.qml:404
msgid "Not Attending"
msgstr ""

#: src/qml/EventDetails.qml:406
msgid "Maybe"
msgstr ""

#: src/qml/EventDetails.qml:408
msgid "No Reply"
msgstr ""

#: src/qml/EventDetails.qml:447 src/qml/NewEvent.qml:637
msgid "Description"
msgstr ""

#: src/qml/EventDetails.qml:474 src/qml/NewEvent.qml:872
#: src/qml/NewEvent.qml:889
msgid "Reminder"
msgstr ""

#: src/qml/EventListModel.qml:99
msgid "Birthdays & Anniversaries"
msgstr ""

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the page to choose repetition
#. and as the header of the list item that shows the repetition
#. summary in the page that displays the event details
#: src/qml/EventRepetition.qml:40 src/qml/EventRepetition.qml:179
msgid "Repeat"
msgstr ""

#: src/qml/EventRepetition.qml:199
msgid "Repeats On:"
msgstr ""

#: src/qml/EventRepetition.qml:245
msgid "Interval of recurrence"
msgstr ""

#: src/qml/EventRepetition.qml:270
msgid "Recurring event ends"
msgstr ""

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the option selector to choose
#. its repetition
#: src/qml/EventRepetition.qml:294 src/qml/NewEvent.qml:845
msgid "Repeats"
msgstr ""

#: src/qml/EventRepetition.qml:320 src/qml/NewEvent.qml:499
msgid "Date"
msgstr ""

#. TRANSLATORS: the argument refers to multiple recurrence of event with count .
#. E.g. "Daily; 5 times."
#: src/qml/EventUtils.qml:75
msgid "%1; %2 time"
msgid_plural "%1; %2 times"
msgstr[0] ""
msgstr[1] ""

#. TRANSLATORS: the argument refers to recurrence until user selected date.
#. E.g. "Daily; until 12/12/2014."
#: src/qml/EventUtils.qml:79
msgid "%1; until %2"
msgstr ""

#: src/qml/EventUtils.qml:93
msgid "; every %1 days"
msgstr ""

#: src/qml/EventUtils.qml:95
msgid "; every %1 weeks"
msgstr ""

#: src/qml/EventUtils.qml:97
msgid "; every %1 months"
msgstr ""

#: src/qml/EventUtils.qml:99
msgid "; every %1 years"
msgstr ""

#. TRANSLATORS: the argument refers to several different days of the week.
#. E.g. "Weekly on Mondays, Tuesdays"
#: src/qml/EventUtils.qml:125
msgid "Weekly on %1"
msgstr ""

#: src/qml/LimitLabelModel.qml:25
msgid "Never"
msgstr ""

#: src/qml/LimitLabelModel.qml:26
msgid "After X Occurrence"
msgstr ""

#: src/qml/LimitLabelModel.qml:27
msgid "After Date"
msgstr ""

#. TRANSLATORS: This is shown in the month view as "Wk" as a title
#. to indicate the week numbers. It should be a max of up to 3 characters.
#: src/qml/MonthComponent.qml:294
msgid "Wk"
msgstr ""

#: src/qml/MonthView.qml:79 src/qml/WeekView.qml:140
msgid "%1 %2"
msgstr ""

#: src/qml/NewEvent.qml:206
msgid "End time can't be before start time"
msgstr ""

#: src/qml/NewEvent.qml:394 src/qml/NewEventBottomEdge.qml:54
msgid "New Event"
msgstr ""

#: src/qml/NewEvent.qml:424
msgid "Save"
msgstr ""

#: src/qml/NewEvent.qml:435
msgid "Error"
msgstr ""

#: src/qml/NewEvent.qml:437
msgid "OK"
msgstr ""

#: src/qml/NewEvent.qml:499
msgid "From"
msgstr ""

#: src/qml/NewEvent.qml:524
msgid "To"
msgstr ""

#: src/qml/NewEvent.qml:553
msgid "All day event"
msgstr ""

#: src/qml/NewEvent.qml:590
msgid "Event Name"
msgstr ""

#: src/qml/NewEvent.qml:610
msgid "More details"
msgstr ""

#: src/qml/NewEvent.qml:661
msgid "Location"
msgstr ""

#: src/qml/NewEvent.qml:749
msgid "Guests"
msgstr ""

#: src/qml/NewEvent.qml:759
msgid "Add Guest"
msgstr ""

#: src/qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr ""

#: src/qml/RecurrenceLabelDefines.qml:23
msgid "Once"
msgstr ""

#: src/qml/RecurrenceLabelDefines.qml:24
msgid "Daily"
msgstr ""

#: src/qml/RecurrenceLabelDefines.qml:25
msgid "On Weekdays"
msgstr ""

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday, Tuesday, Thursday"
#: src/qml/RecurrenceLabelDefines.qml:27
msgid "On %1, %2 ,%3"
msgstr ""

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday and Thursday"
#: src/qml/RecurrenceLabelDefines.qml:29
msgid "On %1 and %2"
msgstr ""

#: src/qml/RecurrenceLabelDefines.qml:30
msgid "Weekly"
msgstr ""

#: src/qml/RecurrenceLabelDefines.qml:31
msgid "Monthly"
msgstr ""

#: src/qml/RecurrenceLabelDefines.qml:32
msgid "Yearly"
msgstr ""

#: src/qml/RemindersModel.qml:31 src/qml/RemindersModel.qml:99
msgid "No Reminder"
msgstr ""

#. TRANSLATORS: this refers to when a reminder should be shown as a notification
#. in the indicators. "On Event" means that it will be shown right at the time
#. the event starts, not any time before
#: src/qml/RemindersModel.qml:34 src/qml/RemindersModel.qml:103
msgid "On Event"
msgstr ""

#: src/qml/RemindersModel.qml:43 src/qml/RemindersModel.qml:112
msgid "%1 week"
msgid_plural "%1 weeks"
msgstr[0] ""
msgstr[1] ""

#: src/qml/RemindersModel.qml:54 src/qml/RemindersModel.qml:110
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] ""
msgstr[1] ""

#: src/qml/RemindersModel.qml:65 src/qml/RemindersModel.qml:108
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] ""
msgstr[1] ""

#: src/qml/RemindersModel.qml:74
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] ""
msgstr[1] ""

#: src/qml/RemindersModel.qml:104 src/qml/RemindersModel.qml:105
#: src/qml/RemindersModel.qml:106 src/qml/RemindersModel.qml:107
#: src/qml/SettingsPage.qml:304 src/qml/SettingsPage.qml:305
#: src/qml/SettingsPage.qml:306 src/qml/SettingsPage.qml:307
#: src/qml/SettingsPage.qml:308 src/qml/SettingsPage.qml:309
#: src/qml/SettingsPage.qml:310 src/qml/SettingsPage.qml:311
msgid "%1 minutes"
msgstr ""

#: src/qml/RemindersModel.qml:109
msgid "%1 hours"
msgstr ""

#: src/qml/RemindersModel.qml:111
msgid "%1 days"
msgstr ""

#: src/qml/RemindersModel.qml:113
msgid "%1 weeks"
msgstr ""

#: src/qml/RemindersModel.qml:114
msgid "Custom"
msgstr ""

#: src/qml/RemindersPage.qml:62
msgid "Custom reminder"
msgstr ""

#: src/qml/RemindersPage.qml:73
msgid "Set reminder"
msgstr ""

#: src/qml/SettingsPage.qml:85
msgid "Show week numbers"
msgstr ""

#: src/qml/SettingsPage.qml:104
msgid "Display Chinese calendar"
msgstr ""

#: src/qml/SettingsPage.qml:125
msgid "Business hours"
msgstr ""

#: src/qml/SettingsPage.qml:235
msgid "Default reminder"
msgstr ""

#: src/qml/SettingsPage.qml:282
msgid "Default length of new event"
msgstr ""

#: src/qml/SettingsPage.qml:345
msgid "Default calendar"
msgstr ""

#: src/qml/SettingsPage.qml:414
msgid "Theme"
msgstr ""

#: src/qml/SettingsPage.qml:437
msgid "System theme"
msgstr ""

#: src/qml/SettingsPage.qml:438
msgid "SuruDark theme"
msgstr ""

#: src/qml/SettingsPage.qml:439
msgid "Ambiance theme"
msgstr ""

#. TRANSLATORS: W refers to Week, followed by the actual week number (%1)
#: src/qml/TimeLineHeader.qml:54
msgid "W%1"
msgstr ""

#: src/qml/WeekView.qml:147 src/qml/WeekView.qml:148
msgid "MMM"
msgstr ""

#: src/qml/YearView.qml:83
msgid "Year %1"
msgstr ""

#: src/qml/calendar.qml:102
msgid ""
"Calendar app accept four arguments: --starttime, --endtime, --newevent and --"
"eventid. They will be managed by system. See the source for a full comment "
"about them"
msgstr ""

#: src/qml/calendar.qml:394 src/qml/calendar.qml:580
msgid "Day"
msgstr ""

#: src/qml/calendar.qml:402 src/qml/calendar.qml:593
msgid "Week"
msgstr ""

#: src/qml/calendar.qml:410 src/qml/calendar.qml:606
msgid "Month"
msgstr ""

#: src/qml/calendar.qml:418 src/qml/calendar.qml:619
msgid "Year"
msgstr ""
